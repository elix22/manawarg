TARGET = manawarg

QT += core gui widgets

LIBS += \
    $${PWD}/Urho3D/lib/libUrho3D.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++17 -O2

INCLUDEPATH += \
    Urho3D/include \
    Urho3D/include/Urho3D/ThirdParty \

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

HEADERS += \
    weaver.h \
    qocoon.h \
    urho3d.h \
    view3d.h \
    materialeditor.h \
    colorwidget.h \
    urhodockwidget.h \
    urhowidget.h \
    propertieswidget.h \
    texteditor.h \
    propertyrow.h \
    texturewidget.h

SOURCES += \
    main.cpp \
    weaver.cpp \
    qocoon.cpp \
    view3d.cpp \
    materialeditor.cpp \
    colorwidget.cpp \
    urhodockwidget.cpp \
    urhowidget.cpp \
    propertieswidget.cpp \
    texteditor.cpp \
    propertyrow.cpp \
    texturewidget.cpp

DISTFILES += \
    LICENSE_TEMPLATE

RESOURCES += \
    resources.qrc
