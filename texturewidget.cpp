/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#include <QHBoxLayout>
#include "weaver.h"

#include "texturewidget.h"

TextureWidget::TextureWidget(TextureUnit textureUnit) : QWidget(),
    textureButton_{ new QPushButton(this) },
    texture_{ nullptr },
    textureUnit_{ textureUnit }
{
    QHBoxLayout* layout{ new QHBoxLayout(this) };
    layout->setMargin(0);

    textureButton_->setMaximumSize(512, 32);
    textureButton_->setMinimumSize(32, 32);
    textureButton_->setIconSize(QSize(512, 30));
    textureButton_->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
    layout->addWidget(textureButton_);

    setLayout(layout);

    updateTextureButton();

    connect(textureButton_, SIGNAL(clicked(bool)), this, SLOT(pickTexture()));
}

void TextureWidget::setTexture(Texture* texture)
{
    if (texture == texture_)
        return;

    texture_ = texture;

    updateTextureButton();
}

void TextureWidget::updateTextureButton()
{
    if (!texture_) {

        textureButton_->setText("Texture");
        QString unitName{ toQString(textureUnitNames[textureUnit_]) };
        if (!unitName.isEmpty())
            unitName = unitName.append(" ");
        textureButton_->setToolTip("No " + unitName + "texture set");
        textureButton_->setIcon(QIcon());
        return;
    }

    textureButton_->setText("");
    textureButton_->setToolTip(toQString(texture_->GetName()));

    QPixmap pixmap{ textureButton_->iconSize() };
    Weaver::paintCheckerboard(&pixmap);
    Image* image{ static_cast<Texture2D*>(texture_)->GetImage() };

    if (image) {

        QPixmap texturePixmap{ toPixmap(image) };
        QPainter p{ &pixmap };
        p.drawPixmap(pixmap.rect(), texturePixmap);
        p.end();
    }

    textureButton_->setIconSize(pixmap.size());
    textureButton_->setIcon(QIcon(pixmap));
}

void TextureWidget::resizeEvent(QResizeEvent* event)
{
    textureButton_->setIconSize(size());
}

void TextureWidget::pickTexture()
{

}
