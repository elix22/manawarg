/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#ifndef MATERIALEDITOR_H
#define MATERIALEDITOR_H

#include <QAction>
#include <QSplitter>
#include <QGroupBox>
#include <QScrollArea>
#include <QLineEdit>
#include <QFormLayout>

#include "view3d.h"
#include "propertieswidget.h"

#include "urhowidget.h"

class FlipSplitter : public QSplitter
{
    Q_OBJECT
public:
    FlipSplitter(QWidget* parent = nullptr);

public slots:
    void resizeEvent(QResizeEvent* event) override;
};

///

class MaterialEditor : public UrhoWidget
{
    Q_OBJECT
    URHO3D_OBJECT(MaterialEditor, UrhoWidget)

public:
    explicit MaterialEditor(Context* context, QWidget *parent = nullptr);

    void setOrientation(Qt::Orientation orientation) override;

    void openMaterial(const String& materialFileName);
    void setMaterial(Material* material);
    void setTechnique(Technique* technique);

public slots:
    void showEvent(QShowEvent*) override;
    void updateMaterial(QObject* sender);

protected slots:
    void actionNew() override;
    void actionOpen() override;
    void actionSave() override;

private slots:
    void showPreviewMenu(const QPoint& pos);
    void pickBackgroundColor();
    void updateBackgroundColor();

    void setPreviewModel();
private:
    void createPreviewScene();

    View3D* materialView_;
    FlipSplitter* splitter_;
    QVBoxLayout* bottomLayout_;
    PropertiesWidget* propertiesWidget_;

    Scene* scene_;
    Camera* camera_;
    StaticModel* previewModel_;
    SharedPtr<Material> material_;
    Color backgroundColor_;
    void updatePropertiesVisibility();
};

#endif // MATERIALEDITOR_H
