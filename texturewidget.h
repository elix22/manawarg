/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#ifndef TEXTUREWIDGET_H
#define TEXTUREWIDGET_H

#include <QPushButton>
#include "urho3d.h"

#include <QWidget>

static const std::vector<String> textureUnitNames {

    "diffuse",
    "normal",
    "specular",
    "emissive",
    "environment",
    "volume",
    "custom1",
    "custom2",
    "lightramp",
    "lightshape",
    "shadowmap",
    "faceselect",
    "indirection",
    "depth",
    "light",
    "zone"
};

class TextureWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TextureWidget(TextureUnit textureUnit = TU_DIFFUSE);

    void setTexture(Texture* texture);
    Texture* texture() { return texture_; }
    TextureUnit textureUnit() { return textureUnit_; }

public slots:
    void updateTextureButton();

signals:
    void textureChanged();

protected:
    void resizeEvent(QResizeEvent *event);

private slots:
    void pickTexture();

private:
    QPushButton* textureButton_;
    Texture* texture_;
    TextureUnit textureUnit_;
};

#endif // TEXTUREWIDGET_H
