/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#include <QTextEdit>
#include "texteditor.h"

TextEditor::TextEditor(Context* context, QWidget* parent) : UrhoWidget(context, parent),
    mainLayout_{ new QVBoxLayout() }
{
    setObjectName("Text Editor");


    createFileButtons();
    mainLayout_->addWidget(fileButtonRow_);
    mainLayout_->setAlignment(fileButtonRow_, Qt::AlignRight | Qt::AlignTop);

    QTextEdit* textEdit{ new QTextEdit() };

    QFont font{};
    font.setFamily("Monospace");
    textEdit->setCurrentFont(font);
    textEdit->setFont(font);
    mainLayout_->addWidget(textEdit);

    setLayout(mainLayout_);
}

void TextEditor::setOrientation(Qt::Orientation orientation)
{
    if (orientation == Qt::Vertical) {

        mainLayout_->setDirection(QBoxLayout::TopToBottom);

    } else {

        mainLayout_->setDirection(QBoxLayout::LeftToRight);
    }

    UrhoWidget::setOrientation(orientation);
}

void TextEditor::actionNew()
{

}

void TextEditor::actionOpen()
{

}

void TextEditor::actionSave()
{

}

