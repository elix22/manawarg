/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/


#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "urho3d.h"
#include <QApplication>

#include <QPixmap>
#include <QPainter>

#define CONFIGURATIONFILENAME "manawarg.conf"

class Qocoon;

class Weaver : public QApplication,  public Application
{
    Q_OBJECT
    URHO3D_OBJECT(Weaver, Application)
public:
    Weaver(Context* context, int & argc, char** argv);
    virtual ~Weaver();


    void createScene();

    String locateResourcesRoot(String path, bool acceptUrhoFolder);
    String urhoFolder() const;
    String urhoDataFolder() const;
    String urhoCoreDataFolder() const;
    String projectLocation() const;

    void Setup() override;
    void Start() override;
    void Stop()  override;
    void exit();
    void runFrame();

    Scene* getScene() const { return scene_; }
    bool inResourceFolder(const String& fileName) const {

        const String path{ GetPath(fileName) };

        return path.Contains(urhoDataFolder())
            || path.Contains(urhoCoreDataFolder())
            || (!projectLocation().Empty() && path.Contains(projectLocation()));
    }
    String trimmedResourceName(String fileName) {

        if (!inResourceFolder(fileName))

            return "";

        else

            return fileName.Replaced(containingFolder(fileName), "");

    }
    const String containingFolder(const String& path) {

        if (path.Contains(projectLocation()))

            return AddTrailingSlash(projectLocation());

        else if (path.Contains(urhoDataFolder()))

            return AddTrailingSlash(urhoDataFolder());

        else if (path.Contains(urhoCoreDataFolder()))

            return AddTrailingSlash(urhoCoreDataFolder());

        return "";

    }
    static void paintCheckerboard(QPaintDevice* paintDevice, int squareSize = 8)
    {
        if (squareSize == 0)
            return;

        QPixmap checkerboard{ 2 * squareSize, 2 * squareSize };
        checkerboard.fill(QColor("#3a3b3c"));
        QPainter cP{ &checkerboard };
        cP.setBrush(QColor("#acabad"));
        cP.setPen(Qt::transparent);
        for (bool second: {false, true}) {

            cP.drawRect(second * squareSize, second * squareSize, squareSize, squareSize);
        }
        cP.end();

        QPainter p{ paintDevice };
        p.setBrush(QBrush(checkerboard));
        p.setPen(QPen(QColor("#555555"), 2));
        p.drawRect(QRect(0, 0, paintDevice->width(), paintDevice->height()));
        p.end();
    }

public slots:
    void onTimeout();
    void requestUpdate() { requireUpdate_ = true; }

private:
    void handleArgument();
    void findUrho();
    bool looksLikeUrho(QString urhoFolder);
    bool looksLikeUrho(String urhoFolder);

    String argument_;
    bool requireUpdate_;
    Qocoon* mainWindow_;
    Scene* scene_;
    SharedPtr<XMLFile> project_;
};

static QString toQString(String string) {

    return QString{ string.CString() };
}
static String toString(QString qString) {

    return String{ qString.toLatin1().data() };
}
static QColor toQColor(const Color color, bool alpha = true) {

    return QColor::fromRgbF( std::min(std::max(0.0f, color.r_), 1.0f),
                             std::min(std::max(0.0f, color.g_), 1.0f),
                             std::min(std::max(0.0f, color.b_), 1.0f),
                    (alpha ? std::min(std::max(0.0f, color.a_), 1.0f) : 1.0f) );
}
static QPixmap toPixmap(Image* image) {

    return QPixmap::fromImage(QImage{
                  image->GetData(),
                  image->GetWidth(),
                  image->GetHeight(),
                  QImage::Format_RGBA8888 });
}

#endif // MASTERCONTROL_H
