/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#include "materialeditor.h"
#include "colorwidget.h"
#include "texturewidget.h"
#include "propertieswidget.h"

#include "propertyrow.h"
#include "weaver.h"

PropertyRow::PropertyRow(QString text, PropertyType type, PropertiesWidget* parent): QWidget(parent),
    propertiesWidget_{ parent },
    type_{ type },
    label_{ new QLabel(text) },
    layout_{ new QHBoxLayout() }
{
    label_->setObjectName(text);

    switch (type_) {
    case PT_Technique: {

        createTechniqueWidgets();
        break;

    } case PT_Color: {
        createColorWidget(text);
        break;

    } case PT_ColorAndTexture: {
        createColorWidget(text);
    } case PT_Texture: {
        createTextureWidget(text);
        break;

    } default:
        break;
    }
}

QWidget* PropertyRow::propertyWidget(PropertyType propertyType) const
{

    switch (propertyType) {
    default: case PT_ColorAndTexture:
        return nullptr;

    case PT_Technique: case PT_Color:
        return layout()->itemAt(0)->widget();
        break;
    case PT_Texture:
        if (type() == PT_ColorAndTexture)
            return layout()->itemAt(2)->widget();
        else
            return layout()->itemAt(0)->widget();
        break;
    }
}

void PropertyRow::createTechniqueWidgets()
{
    QString resourceTypeName{ "technique" };

    QLineEdit* techniqueEdit{ new QLineEdit() };
    techniqueEdit->setReadOnly(true);
    techniqueEdit->setAlignment(Qt::AlignCenter);
    layout_->addWidget(techniqueEdit);
    QPushButton* openResourceButton{ new QPushButton() };
    openResourceButton->setIcon(QIcon(":/Open"));
    openResourceButton->setToolTip("Pick " + resourceTypeName );
    layout_->addWidget(openResourceButton);

    connect(openResourceButton, SIGNAL(clicked(bool)), propertiesWidget_, SLOT(pickTechnique()));
}
void PropertyRow::createColorWidget(QString text)
{
    ColorWidget* colorWidget{ new ColorWidget(text, propertiesWidget_->urhoWidget()) };

    layout_->addWidget(colorWidget);
    layout_->setStretchFactor(colorWidget, 0);
    layout_->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::MinimumExpanding));

    connect(colorWidget, SIGNAL(colorChanged()), propertiesWidget_, SLOT(propertyChanged()));
}
void PropertyRow::createTextureWidget(QString unitName)
{
    TextureUnit textureUnit{ unitFromName(toString(unitName)) };
    TextureWidget* textureWidget{ new TextureWidget(textureUnit) };
    textureWidget->setProperty("name", unitName + " Texture");
    textureWidget->setProperty("parameter", unitName.toLower());

    if (!layout_->count())
        layout_->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::MinimumExpanding));

    layout_->addWidget(textureWidget);
    layout_->setStretchFactor(textureWidget, 1);

    connect(textureWidget, SIGNAL(textureChanged()), propertiesWidget_, SLOT(propertyChanged()));
}

TextureUnit PropertyRow::unitFromName(String name)
{
    if (name.Empty())
        return MAX_TEXTURE_UNITS;

    name = name.ToLower();

    for (unsigned t{0}; t < textureUnitNames.size(); ++t) {

        if (textureUnitNames[t] == name) {

            return static_cast<TextureUnit>(t);
        }
    }

    return MAX_TEXTURE_UNITS;
}
