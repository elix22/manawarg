/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#include <QResizeEvent>
#include <QPushButton>
#include <QFileDialog>
#include <QMessageBox>
#include <QMenu>
#include <QTimer>
#include "colorwidget.h"
#include "texturewidget.h"
#include "weaver.h"

#include "materialeditor.h"

MaterialEditor::MaterialEditor(Context* context, QWidget* parent) : UrhoWidget(context, parent),
    materialView_{      new View3D(context_) },
    splitter_{          new FlipSplitter() },
    bottomLayout_{      new QVBoxLayout() },
    propertiesWidget_{  new PropertiesWidget(context_, this) },
    scene_{ nullptr },
    previewModel_{},
    backgroundColor_{ Color::BLACK }
{
    setObjectName("Material Editor");
    for (QAction* operation: fileOperations_)
        operation->setText(operation->text() + " Material");

    QVBoxLayout* mainLayout{ new QVBoxLayout() };
    mainLayout->setMargin(1);

    createPreviewScene();
    materialView_->setScene(scene_);
    materialView_->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(materialView_, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showPreviewMenu(const QPoint&)));

    splitter_->addWidget(materialView_);
    splitter_->setCollapsible(0, false);

    QWidget* bottomWidget{ new QWidget() };
    bottomLayout_->setMargin(1);
    bottomLayout_->setDirection(QBoxLayout::LeftToRight);

    createFileButtons();
    bottomLayout_->addWidget(fileButtonRow_);
    bottomLayout_->setAlignment(fileButtonRow_, Qt::AlignRight | Qt::AlignTop);

    bottomLayout_->addWidget(propertiesWidget_);

    pickerBox_ = new QGroupBox();
    pickerBox_->setLayout(new QVBoxLayout());
    pickerBox_->setVisible(false);
    pickerBox_->setStyleSheet("QGroupBox { border: 1px solid rgba(0, 0, 0, 0.23); border-radius: 2px; margin: 1px; }"
                              "QGroupBox::title { background-color: transparent; }");

    bottomWidget->setLayout(bottomLayout_);
    FlipSplitter* bottomSplitter{ new FlipSplitter() };
    bottomSplitter->addWidget(bottomWidget);
    bottomSplitter->addWidget(pickerBox_);
    bottomSplitter->setOrientation(Qt::Vertical);

    splitter_->addWidget(bottomSplitter);
    splitter_->setStretchFactor(0, 0);
    splitter_->setStretchFactor(1, 1);

    mainLayout->addWidget(splitter_);
    setLayout(mainLayout);

    setOrientation(Qt::Vertical);
}

void MaterialEditor::showEvent(QShowEvent*)
{
    bool horizontal{ splitter_->orientation() == Qt::Horizontal };
    int max{ (horizontal ? height() : width()) };

    if (horizontal) {

        materialView_->resize({ std::min(max, materialView_->width()),
                                materialView_->height() });
    } else {

        materialView_->resize({ materialView_->width(),
                                std::min(max, materialView_->height()) });
    }

    if (!splitter_->widget(1)->visibleRegion().isEmpty()) {

        int sizesSum{ splitter_->sizes()[0] + splitter_->sizes()[1] };
        int firstSize{ (splitter_->orientation() == Qt::Horizontal
                        ? materialView_->width() : materialView_->height()) };

        splitter_->setSizes({ firstSize, sizesSum - firstSize });
    }
}

void MaterialEditor::setOrientation(Qt::Orientation orientation)
{
    if (orientation == Qt::Vertical) {

        bottomLayout_->setDirection(QBoxLayout::TopToBottom);

    } else {

        bottomLayout_->setDirection(QBoxLayout::LeftToRight);
    }

    UrhoWidget::setOrientation(orientation);
}

void MaterialEditor::createPreviewScene()
{
    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();

    //Zone
    scene_->CreateComponent<Zone>();
    //Light
    Node* lightNode{ scene_->CreateChild("Light") };
    lightNode->SetPosition(Vector3(2.0f, 3.0f, 1.0f));
    lightNode->CreateComponent<Light>();
    //Camera
    Node* cameraNode{ scene_->CreateChild("Camera") };
    cameraNode->SetPosition(Vector3::ONE);
    cameraNode->LookAt(Vector3::ZERO);
    cameraNode->CreateComponent<Camera>()->SetFov(36.0f);
    //Preview model
    Node* modelNode{ scene_->CreateChild("Model") };
    modelNode->SetTemporary(true);
    previewModel_ = modelNode->CreateComponent<StaticModel>();
    previewModel_->SetModel(GetSubsystem<ResourceCache>()->GetResource<Model>("Models/Triacontahedron.mdl"));
    previewModel_->SetEnabled(false);
}
void MaterialEditor::showPreviewMenu(const QPoint& pos)
{
    QPoint globalPos{ mapToGlobal(pos) };

    QMenu browserMenu{};

    browserMenu.addAction("Background color", this, SLOT(pickBackgroundColor()));
    QMenu* modelMenu{ new QMenu("Preview model") };

    for (QString modelName: { "Cube", "Sphere", "Triacontahedron" }) {

        QAction* action{ new QAction(modelName) };
        action->setCheckable(true);

        if (modelName == "Cube")
            modelName = "Box";

        action->setObjectName(modelName);
        connect(action, SIGNAL(triggered(bool)), this, SLOT(setPreviewModel()));

        Model* currentModel{ previewModel_->GetModel() };
        if (currentModel)
            action->setChecked(currentModel->GetName().Contains(toString(modelName)));

        modelMenu->addActions({ action });
    }
    browserMenu.addMenu(modelMenu);
    browserMenu.exec(globalPos);
}
void MaterialEditor::pickBackgroundColor()
{
    if (pickerBox_) {

        QLayout* pickerLayout{ pickerBox_->layout() };
        QLayoutItem* item{ pickerLayout->itemAt(0) };

        if (item) {

            pickerLayout->removeItem(item);
            delete item->widget();
        }

        ColorPicker* backgroundPicker{ new ColorPicker(backgroundColor_) };
        pickerLayout->addWidget(backgroundPicker);
        QString colorName{ "Preview Background Colour" };
        pickerBox_->setObjectName(colorName);
        pickerBox_->setTitle(colorName);
        pickerBox_->show();

        connect(backgroundPicker, SIGNAL(colorChanged()), this, SLOT(updateBackgroundColor()));
    }
}
void MaterialEditor::setPreviewModel()
{
    String name{ toString(sender()->objectName()) };
    previewModel_->GetNode()->SetScale(name == "Box" ? 0.666f : 1.0f);
    previewModel_->SetModel(GetSubsystem<ResourceCache>()->GetResource<Model>("Models/" + name + ".mdl"));

    setMaterial(material_);
}

void MaterialEditor::setMaterial(Material* material)
{
    material_ = material;
    previewModel_->SetMaterial(material_);
    previewModel_->SetEnabled(material_);

    propertiesWidget_->setResource(material);

    materialView_->updateView();
}
void MaterialEditor::setTechnique(Technique* technique)
{
    material_->SetTechnique(0, technique);

    materialView_->updateView();
}

void MaterialEditor::openMaterial(const String& materialFileName) //Needs generalization for XML-based resources
{
    if (materialFileName.Empty()) {

        return;

    } else {

        ResourceCache* cache{ GetSubsystem<ResourceCache>() };
        SharedPtr<XMLFile> xmlFile{ cache->GetTempResource<XMLFile>(materialFileName) };

        if (xmlFile) {

            const XMLElement& rootElem{ xmlFile->GetRoot("material") };

            if (rootElem.IsNull()) {

                QString elemName{ toQString(xmlFile->GetRoot().GetName()) };
                QString message{ "Could not load " };

                if (!elemName.isEmpty())
                    message += elemName + " as ";

                message += "material";

                QMessageBox* messageBox{ new QMessageBox(QMessageBox::Warning, "That failed", message )};
                messageBox->setWindowIcon(QIcon(":/Icon"));
                messageBox->exec();

                return;
            }

            Weaver* weaver{ GetSubsystem<Weaver>() };

            if (weaver->inResourceFolder(materialFileName)) {

                SharedPtr<Material> material{ GetSubsystem<ResourceCache>()->GetResource<Material>(materialFileName) };

                if (material) {

                    String materialName{ weaver->trimmedResourceName(materialFileName) };
                    material->SetName(materialName);
                    GetSubsystem<ResourceCache>()->ReloadResource(material);
                    setMaterial(material);

                    Log::Write(LOG_INFO, materialName);

                }  else {

                    //Failed to load material
                }
            } else {

                if (weaver->projectLocation() == "") {

                    if (!weaver->locateResourcesRoot(materialFileName, false).Empty())
                        openMaterial(materialFileName);
                } else {

                    QMessageBox* messageBox{ new QMessageBox(QMessageBox::Warning, "Out of scope",
                                                             "Material exists outside current resource structure")};
                    messageBox->setWindowIcon(QIcon(":/Icon"));
                    messageBox->exec();
                }
                //Resource not in project tree, what to do? Add folder/Switch project | Copy resource file
            }
        }
    }
}

void MaterialEditor::actionNew()
{
    Material* material{new Material(context_)};

//    Technique* technique{ GetSubsystem<ResourceCache>()->GetResource<Technique>("Techniques/NoTexture.xml") };

//    for (Pass* p: technique->GetPasses()) {

//        Log::Write(LOG_INFO, p->GetName());
//    }
//    for (MaterialShaderParameter& p: material->GetShaderParameters().Values()) {

//        Log::Write(LOG_INFO, p.name_);
//        Log::Write(LOG_INFO, String(p.value_));

//    }
    setMaterial(material);

    materialView_->updateView();
}
void MaterialEditor::actionOpen()
{
    Weaver* weaver{ GetSubsystem<Weaver>() };
    String projectFolder{ weaver->projectLocation() };
    String urhoDataFolder{ weaver->urhoDataFolder() };

    QString xmlFilter{ tr("*.xml") };
    String materialFileName{
        toString(QFileDialog::getOpenFileName(this, tr("Open Material"),
                                              toQString(projectFolder.Empty() ? urhoDataFolder : projectFolder),
                                              xmlFilter, &xmlFilter)) };
    openMaterial(materialFileName);
}
void MaterialEditor::actionSave()
{
    if (!material_ || material_->GetName().Empty())
        return;


    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    String fullName{ cache->GetResourceFileName(material_->GetName()) };
    File saveFile{ context_, fullName, FILE_WRITE };
    material_->Save(saveFile);
}

void MaterialEditor::updateMaterial(QObject* sender)
{
    String parameterName{ toString(sender->property("parameter").toString()) };

    if (auto cw = qobject_cast<ColorWidget*>(sender)) {

        material_->SetShaderParameter(parameterName, cw->color().ToVector4());

    } else if (auto tw = qobject_cast<TextureWidget*>(sender)) {

        material_->SetTexture(tw->textureUnit(), tw->texture());
    }

    materialView_->updateView();
}
void MaterialEditor::updateBackgroundColor()
{
    if (!scene_)
        return;

    scene_->GetComponent<Zone>()->SetFogColor(backgroundColor_);
    materialView_->setRenderPath("RenderPaths/Forward" + String(backgroundColor_.a_ == 1.0f ? ".xml" : "Transparent.xml"));
    materialView_->updateView();
}

FlipSplitter::FlipSplitter(QWidget* parent): QSplitter(parent) {}
void FlipSplitter::resizeEvent(QResizeEvent* event)
{
    double ratio{ static_cast<double>(event->size().width()) / event->size().height() };

    Qt::Orientation o{ orientation() };

    if (ratio > 1.1)

        o = Qt::Horizontal;

    else if (ratio < 0.9)

        o = Qt::Vertical;

    if (orientation() != o)
        setOrientation(o);

    QSplitter::resizeEvent(event);
}
