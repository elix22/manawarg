/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#ifndef COLORWIDGET_H
#define COLORWIDGET_H

#include "urho3d.h"
#include <QFormLayout>
#include <QPushButton>
#include <QLabel>

#include <QWidget>

#define SPECPOWER 4.0f

class UrhoWidget;

class ColorWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ColorWidget(QString name, UrhoWidget* parent = nullptr);

    void setColor(Color color);
    Color& color() { return color_; }

    bool diffuse() const;
    bool specular() const;
    bool emissive() const;

public slots:
    void updateColorButton();

signals:
    void colorChanged();
    void pickerShown();

private slots:
    void pickColor();

private:
    UrhoWidget* urhoWidget_;
    QPushButton* colorButton_;
    Color color_;
};

#include <QSlider>
#include <QTabBar>

enum ColorEnum{ RED, GREEN, BLUE, ALPHA };
enum ColorMode{ RGB, HSV };

class ColorPicker : public QWidget
{
    Q_OBJECT
public:
    explicit ColorPicker(ColorWidget* parent);
    explicit ColorPicker(Color& color);

public slots:
    void updateColor();

    void resizeEvent(QResizeEvent*) override;
    void paintEvent(QPaintEvent*) override;

signals:
    void colorChanged();

private slots:
    void updateSliderValues();
    void changeMode();

private:
    static ColorMode lastPickedMode_;
    ColorWidget* colorWidget_;
    QVBoxLayout* mainLayout_;
    QFormLayout* formLayout_;
    Color& color_;
    QLabel* colorLabel_;
    QTabBar* tabBar_;

    std::map<int, QSlider*> sliders_;
    std::map<int, QLabel*> labels_;

    void createMainLayout();
    void createFormLayout();
    void createSliders();
    void updateToolTips();
    bool modeRGB() { return tabBar_->currentIndex() == 0; }
    bool modeHSV() { return tabBar_->currentIndex() == 1; }
    void updateLabels();
    void createTabBar();
};

#endif // COLORDIALOG_H
