/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#ifndef PROPERTYROW_H
#define PROPERTYROW_H

#include <QLabel>
#include "urhowidget.h"

#include <QWidget>

enum PropertyType{

    PT_Technique,
    PT_Color,
    PT_Texture,
    PT_ColorAndTexture,
    PT_Float,
    PT_Vector2,
    PT_Vector3,
    PT_Model
};

class PropertiesWidget;

class PropertyRow: QWidget
{
    Q_OBJECT
public:
    PropertyRow(QString text, PropertyType type, PropertiesWidget* parent);

    PropertyType type() const { return type_; }

    QLabel* label() const { return label_; }
    QHBoxLayout* layout() const { return layout_; }

    QWidget* propertyWidget(PropertyType propertyType) const;
private:
    void createColorWidget(QString text);
    void createTextureWidget(QString unit);
    TextureUnit unitFromName(String name);

    PropertiesWidget* propertiesWidget_;
    PropertyType type_;
    QLabel* label_;
    QHBoxLayout* layout_;
    void createTechniqueWidgets();
};

#endif // PROPERTYROW_H
