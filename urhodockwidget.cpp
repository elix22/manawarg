/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#include <QSettings>
#include <QComboBox>
#include <QPushButton>
#include <QHBoxLayout>
#include <QResizeEvent>
#include <QPainter>
#include <QApplication>
#include <QDesktopWidget>
#include "materialeditor.h"

#include "urhodockwidget.h"

UrhoDockWidget::UrhoDockWidget(UrhoWidget* widget, QWidget* parent) : QDockWidget(parent)
{
    applyWidget(widget);

    connect(this, SIGNAL(topLevelChanged(bool)), SLOT(onTopLevelChanged(bool)));
}

void UrhoDockWidget::applyWidget(UrhoWidget* newWidget)
{
    if (urhoWidget_ == newWidget)
        return;

    newWidget->setParent(this);
    setWindowTitle(newWidget->objectName());
    urhoWidget_ = newWidget;

    QDockWidget::setWidget(newWidget);
}

void UrhoDockWidget::onTopLevelChanged(bool topLevel)
{
    if (!topLevel)
        return;

    double ratio{ static_cast<double>(width()) / height() };
    double maxRatio{ 2.3 + 1.7 * (ratio > 1.0) };

    if (ratio > maxRatio) {

        resize(height() * maxRatio, height());

    } else if (ratio < 1.0 / maxRatio) {

        resize(width(), width() * maxRatio);
    }
}

void UrhoDockWidget::resizeEvent(QResizeEvent* event)
{
    double ratio{ static_cast<double>(event->size().width()) / event->size().height() };
    double bias{ 0.5 * static_cast<double>(event->size().height()) / QApplication::desktop()->screenGeometry().height() };

    ratio -= bias;

    if (features() & DockWidgetVerticalTitleBar) {

        if (ratio < 1.23) {
            setFeatures(features() ^ DockWidgetVerticalTitleBar);
            if (urhoWidget_)
                urhoWidget_->setOrientation(Qt::Vertical);
        }

    } else {
        if (ratio > 1.42) {

            setFeatures(features() | DockWidgetVerticalTitleBar);

            if (urhoWidget_)
                urhoWidget_->setOrientation(Qt::Horizontal);
        }
    }
}

void UrhoDockWidget::paintEvent(QPaintEvent *event)
{
    QDockWidget::paintEvent(event);

    QPainter p{ this };

    p.setPen(QColor(23, 42, 55, 120));
    p.drawRect(QRect(0, 0, width() - 1, height() - 1));
    p.end();
}
