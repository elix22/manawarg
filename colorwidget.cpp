/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#include <QHBoxLayout>
#include <QDialogButtonBox>
#include <QVariant>
#include <QPainter>
#include <QGraphicsEffect>
#include <QDialog>
#include "urhowidget.h"
#include "materialeditor.h"
#include "weaver.h"

#include "colorwidget.h"

ColorWidget::ColorWidget(QString name, UrhoWidget* parent) : QWidget(parent),
    urhoWidget_{ parent },
    colorButton_{ new QPushButton(this) },
    color_{ Color::YELLOW }
{
    QString fullName{ name + " Color" };
    setProperty("name", fullName);
    setToolTip(fullName);

    if (name != "Emissive")
        name.truncate(4);

    setProperty("parameter", "Mat" + name + "Color");

    QHBoxLayout* mainLayout{ new QHBoxLayout() };
    mainLayout->setMargin(0);

    mainLayout->addWidget(colorButton_);
    colorButton_->setMaximumSize(34, 32);
    colorButton_->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    colorButton_->setIconSize(QSize(30, 30));

    setLayout(mainLayout);

    updateColorButton();

    connect(colorButton_, SIGNAL(clicked(bool)), this, SLOT(pickColor()));
}
bool ColorWidget::diffuse() const
{
    return property("parameter").toString().contains("Diff");
}
bool ColorWidget::specular() const
{
    return property("parameter").toString().contains("Spec");
}
bool ColorWidget::emissive() const
{
    return property("parameter").toString().contains("Emissive");
}
void ColorWidget::setColor(Color color)
{
    color_ = color;
    updateColorButton();
}
void ColorWidget::updateColorButton()
{
    QPixmap colorPixmap{ 24, 24 };

    if (diffuse())
        Weaver::paintCheckerboard(&colorPixmap);

    QPainter p{ &colorPixmap };
    p.fillRect(colorPixmap.rect(), toQColor(color_, diffuse()));
    p.end();

    colorButton_->setIcon(QIcon(colorPixmap));

    emit colorChanged();
}

void ColorWidget::pickColor()
{
    QGroupBox* pickerBox{ urhoWidget_->pickerBox() };

    if (pickerBox) {

        QLayout* pickerLayout{ pickerBox->layout() };
        QLayoutItem* item{ pickerLayout->itemAt(0) };

        if (item) {

            pickerLayout->removeItem(item);
            delete item->widget();
        }

        ColorPicker* picker{ new ColorPicker(this) };
        pickerLayout->addWidget(picker);
        QString colorName{ property("name").toString() };
        pickerBox->setObjectName(colorName);
        pickerBox->setTitle(colorName);
        pickerBox->show();

        connect(picker, SIGNAL(colorChanged()), this, SLOT(updateColorButton()));

    } else {

        QDialog* colorDialog{ new QDialog() };
        QBoxLayout* colorDialogLayout{ new QBoxLayout(QBoxLayout::TopToBottom) };
        colorDialogLayout->addWidget(new ColorPicker(this));
        colorDialog->setLayout(colorDialogLayout);
        colorDialog->exec();
    }
}

///

ColorMode ColorPicker::lastPickedMode_{ RGB };

ColorPicker::ColorPicker(ColorWidget* parent) : QWidget(parent),
    colorWidget_{ parent },
    color_{ colorWidget_->color() },
    colorLabel_{},
    tabBar_{ new QTabBar(this) }
{
    createMainLayout();

    colorLabel_ = new QLabel();
    colorLabel_->setMinimumSize(12, 12);
    colorLabel_->setScaledContents(true);
    mainLayout_->addWidget(colorLabel_);

    createFormLayout();
    createSliders();
    createTabBar();
}
ColorPicker::ColorPicker(Color& color) : QWidget(),
    colorWidget_{ nullptr },
    color_{ color },
    colorLabel_{ nullptr },
    tabBar_{ new QTabBar(this) }
{
    createMainLayout();
    createFormLayout();
    createSliders();
    createTabBar();

}

void ColorPicker::createMainLayout()
{
    mainLayout_ = new QVBoxLayout();
    mainLayout_->addSpacing(12);
    mainLayout_->setMargin(4);
    setLayout(mainLayout_);
    setMinimumWidth(96);
}

void ColorPicker::createTabBar()
{
    tabBar_->addTab("RGB");
    tabBar_->addTab("HSV");
    tabBar_->setExpanding(false);
    tabBar_->setShape(QTabBar::RoundedSouth);
    connect(tabBar_, SIGNAL(currentChanged(int)), this, SLOT(changeMode()));
    tabBar_->setCurrentIndex(lastPickedMode_);
    mainLayout_->addWidget(tabBar_);
}
void ColorPicker::createFormLayout()
{
    formLayout_ = new QFormLayout();
    formLayout_->setMargin(0);
    formLayout_->setSpacing(0);
    formLayout_->setLabelAlignment(Qt::AlignRight);
    mainLayout_->addLayout(formLayout_);
}
void ColorPicker::createSliders()
{
    for (int c{RED}; c <= ALPHA; ++c) {

        QSlider* colorSlider{ new QSlider(Qt::Horizontal) };
        colorSlider->setMinimum(0);
        colorSlider->setMaximum(1000);
        colorSlider->setSingleStep(10);
        colorSlider->setPageStep(100);
        colorSlider->setTickInterval(250);
        colorSlider->setTickPosition(QSlider::TicksBothSides);
        colorSlider->setMinimumHeight(10);

        QString colorName{};
        switch (c) {
        case RED: {
            colorName = "Red";
            break;
        } case GREEN: {
            colorName = "Green";
            break;
        } case BLUE: {
            colorName = "Blue";
            break;
        } case ALPHA: {
            colorName = "Alpha";

            if (colorWidget_) {

                if (colorWidget_->emissive()) {

                    continue;

                } else if (colorWidget_->specular()) {

                    colorName = "Hardness";

                    colorSlider->setSingleStep(100);
                    colorSlider->setPageStep(800);
                    colorSlider->setTickInterval(2000);
                    colorSlider->setMaximum(8000);
                }
            }
            break;
        } default:
            break;
        }
        QGraphicsColorizeEffect* colorize{ new QGraphicsColorizeEffect(colorSlider) };
        colorize->setColor(QColor(colorName.toLower()));
        colorize->setStrength(0.42);
        colorSlider->setGraphicsEffect(colorize);


        QLabel* sliderLabel{ new QLabel(colorName) };
        sliderLabel->setObjectName(colorName);
        formLayout_->addRow(sliderLabel, colorSlider);
        sliders_[c] = colorSlider;
        labels_[c] = sliderLabel;

        updateSliderValues();
        connect(colorSlider, SIGNAL(valueChanged(int)), this, SLOT(updateColor()));
    }
}
void ColorPicker::updateSliderValues()
{

    for (int c{ RED }; c <= ALPHA; ++c) {

        QSlider* colorSlider{ sliders_[c] };
        if (!colorSlider)
            continue;

        disconnect(colorSlider, SIGNAL(valueChanged(int)), this, SLOT(updateColor()));

        switch (c) {
        case RED:   colorSlider->setValue(1000.0f * (modeHSV() ? color_.Hue()            : color_.r_)); break;
        case GREEN: colorSlider->setValue(1000.0f * (modeHSV() ? color_.SaturationHSV()  : color_.g_)); break;
        case BLUE:  colorSlider->setValue(1000.0f * (modeHSV() ? color_.Value()          : color_.b_)); break;
        case ALPHA: colorSlider->setValue(1000.0f * (colorWidget_ && colorWidget_->specular() ? pow(color_.a_, 1 / SPECPOWER): color_.a_)); break;
        default:
            break;
        }

        connect(colorSlider, SIGNAL(valueChanged(int)), this, SLOT(updateColor()));
    }

    updateToolTips();
}
void ColorPicker::updateToolTips()
{
    for (int c{ RED }; c <= ALPHA; ++c) {

        if (!sliders_[c])
            continue;

        switch (c) {
        case RED:   sliders_[c]->setToolTip(QString::number(modeHSV() ? color_.Hue()            : color_.r_, 'd', 3)); break;
        case GREEN: sliders_[c]->setToolTip(QString::number(modeHSV() ? color_.SaturationHSV()  : color_.g_, 'd', 3)); break;
        case BLUE:  sliders_[c]->setToolTip(QString::number(modeHSV() ? color_.Value()          : color_.b_, 'd', 3)); break;
        case ALPHA: sliders_[c]->setToolTip(QString::number(color_.a_, 'd', 3)); break;
        default:
            break;
        }
    }
}
void ColorPicker::updateColor()
{
    for (int c{ RED }; c <= ALPHA; ++c) {

        if (!sliders_[c])
            continue;

        float colorValue{ sliders_[c]->sliderPosition() / 1000.0f };

        if (colorWidget_) {

            if (c == ALPHA && colorWidget_->specular())
                colorValue = pow(colorValue, SPECPOWER);
        }

        switch (c) {
        case RED:   color_.r_ = colorValue; break;
        case GREEN: color_.g_ = colorValue; break;
        case BLUE:  color_.b_ = colorValue; break;
        case ALPHA: color_.a_ = colorValue; break;
        default:
            break;
        }
    }
    if (modeHSV())
        color_.FromHSV(color_.r_, color_.g_, color_.b_, color_.a_);

    updateToolTips();

    emit colorChanged();
}

void ColorPicker::paintEvent(QPaintEvent*)
{
    if (!colorLabel_)
        return;

    QPixmap pixmap{ colorLabel_->size() };
    Weaver::paintCheckerboard(&pixmap, 12);
    QPainter p{ &pixmap } ;
    p.fillRect(QRect(1, 1, pixmap.width() - 2, pixmap.height() - 2),
               toQColor(color_, colorWidget_->diffuse()));
    p.end();
    colorLabel_->setPixmap(pixmap);
}
void ColorPicker::resizeEvent(QResizeEvent*)
{
    for (int c{RED}; c <= ALPHA; ++c) {

        QLabel* l{ labels_[c] };
        if (!l)
            continue;

        QString text{ (width() > 192 ? l->objectName()
                                     : l->objectName().left(1)) };
        if (text != l->text()) {

            l->setText(text);
            l->setAlignment((text.length() == 1 ? Qt::AlignCenter : Qt::AlignRight));
        }
    }
}
void ColorPicker::updateLabels()
{
    for (int c{RED}; c < ALPHA; ++c) {

        QLabel* l{ labels_[c] };

        if (!l)
            continue;

        switch (c) {
        case RED:   l->setObjectName(modeHSV() ? "Hue"          : "Red"    ); break;
        case GREEN: l->setObjectName(modeHSV() ? "Saturation"   : "Green"  ); break;
        case BLUE:  l->setObjectName(modeHSV() ? "Value"        : "Blue"   ); break;
        default:
            break;
        }
    }

    resizeEvent(nullptr);
}

void ColorPicker::changeMode()
{
    lastPickedMode_ = (tabBar_->currentIndex() == RGB ? RGB : HSV);

    for (const auto& [key, slider]: sliders_) {

        if (!slider)
            continue;

        QGraphicsColorizeEffect* effect{ static_cast<QGraphicsColorizeEffect*>(slider->graphicsEffect()) };
        if (!effect)
            continue;

        effect->setEnabled(modeRGB());
    }


    updateSliderValues();
    updateLabels();
}
