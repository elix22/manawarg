/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#ifndef VIEW3DMAGGOT_H
#define VIEW3DMAGGOT_H

#include "urhowidget.h"

#include <QDockWidget>

class UrhoDockWidget : public QDockWidget
{
    Q_OBJECT
public:
    explicit UrhoDockWidget(UrhoWidget* widget, QWidget *parent = nullptr);
    void applyWidget(UrhoWidget* newWidget);

public slots:
    void resizeEvent(QResizeEvent* event) override;
    void paintEvent(QPaintEvent* event) override;

private slots:
    void onTopLevelChanged(bool topLevel);

private:
    UrhoWidget* urhoWidget_;
};

#endif // VIEW3DMAGGOT_H
