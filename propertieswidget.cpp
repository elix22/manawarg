/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#include <QPushButton>
#include <QFileDialog>
#include <QMessageBox>
#include "colorwidget.h"
#include "texturewidget.h"
#include "materialeditor.h"
#include "urhowidget.h"
#include "weaver.h"

#include "propertieswidget.h"

PropertiesWidget::PropertiesWidget(Context* context, UrhoWidget* parent) : QScrollArea(parent), Object(context),
    urhoWidget_{ parent },
    resource_{ nullptr },
    propertiesLayout_{ new QFormLayout(this) },
    containerWidget_{ new QWidget(this) },
    rows_{}
{
    propertiesLayout_->setMargin(4);

    propertiesLayout_->setLabelAlignment(Qt::AlignRight);
    setWidget(containerWidget_);
    setMinimumWidth(128);
}
void PropertiesWidget::resizeEvent(QResizeEvent*)
{
    containerWidget_->resize(QSize(viewport()->width(), containerWidget_->height()));

    updateLabels();
}
void PropertiesWidget::updateLabels()
{
    for (PropertyRow* row: rows_) {

        QLabel* label{ row->label() };
        QString text{ label->objectName() };

        propertiesLayout_->setLabelAlignment(Qt::AlignRight);

        if (width() < 160) {

            text = text.at(0);
            propertiesLayout_->setLabelAlignment(Qt::AlignLeft);

        } else if (width() < 192) {

            if (text.contains("Emis"))

                text = "Emit";

            else

                text = text.left(4);
        }

        if (label->text() != text)

            label->setText(text);
    }
}
void PropertiesWidget::setResource(Resource* resource)
{
    if (resource == resource_)
        return;

    resource_ = resource;
    createPropertyFields();
}
void PropertiesWidget::setPropertiesVisible(bool visible)
{
    containerWidget_->setVisible(visible);
}

void PropertiesWidget::propertyChanged()
{
    if (auto me = static_cast<MaterialEditor*>(urhoWidget_)) {

        me->updateMaterial(sender());
    }
}

void PropertiesWidget::createPropertyFields()
{
    if (resource_->GetTypeInfo()->IsTypeOf<Material>()) {

        createMaterialPropertyFields();
        setPropertiesVisible(true);
    }
}
void PropertiesWidget::createMaterialPropertyFields()
{
    createPropertyRow("Technique", PT_Technique);

    for (QString colorName:{
         "Diffuse",
         "Specular",
         "Emissive" }) {

        createPropertyRow(colorName, PT_ColorAndTexture);
    }

    createPropertyRow("Normal", PT_Texture);


    updatePropertyFields();

}
void PropertiesWidget::createPropertyRow(QString text, PropertyType type)
{
    for (PropertyRow* row: rows_) {

        if (row->label()->objectName() == text && row->type() == type) {

            return;
        }
    }

    PropertyRow* row{ new PropertyRow(text, type, this) };
    rows_.push_back(row);
    propertiesLayout_->addRow(row->label(), row->layout());
}

void PropertiesWidget::pickTechnique()
{
    Weaver* weaver{ GetSubsystem<Weaver>() };
    String projectFolder{ weaver->projectLocation() };
    String urhoFolder{ weaver->urhoFolder() };

    QString xmlFilter{ tr("*.xml") };
    String techniqueFileName{
        toString(QFileDialog::getOpenFileName(this, tr("Pick Technique"),
                                              toQString(projectFolder.Empty() ? urhoFolder : projectFolder),
                                              xmlFilter, &xmlFilter)) };

    if (techniqueFileName.Empty()) {

        return; //Cancelled

    } else {

        ResourceCache* cache{ GetSubsystem<ResourceCache>() };
        SharedPtr<XMLFile> xmlFile{ cache->GetTempResource<XMLFile>(techniqueFileName) };

        if (xmlFile) {

            const XMLElement& rootElem{ xmlFile->GetRoot("technique") };
            const QString errorTitle{ "Could not set technique" };

            if (rootElem.IsNull()) {

                QMessageBox::warning(this, errorTitle, "File not recognized as technique"  );

            } else {
                if (weaver->inResourceFolder(techniqueFileName)) {

                    Technique* technique{ cache->GetResource<Technique>(techniqueFileName) };

                    if (technique) {

                        String techniqueName{ weaver->trimmedResourceName(techniqueFileName) };
                        technique->SetName(techniqueName);
                        cache->ReloadResource(technique);
                        static_cast<MaterialEditor*>(urhoWidget_)->setTechnique(technique);
                        createPropertyFields();

                        Log::Write(LOG_INFO, techniqueName);

                    }  else {

                        QMessageBox::warning(this, errorTitle, "Technique failed to load"  );
                    }
                } else {

                    QMessageBox::warning(this, errorTitle, "Technique exists outside resource tree"  );
                }
            }
        }
    }
}

void PropertiesWidget::updatePropertyFields()
{
    if (resource_->GetTypeInfo()->IsTypeOf<Material>()) {

        Material* material{ static_cast<Material*>(resource_.Get()) };
        SharedPtr<Technique> technique{ material->GetTechnique(0) };
        const HashMap<StringHash, MaterialShaderParameter>& parameters{ material->GetShaderParameters() };

        for (PropertyRow* row: rows_) {

            switch (row->type()) {
            case PT_Technique: {

                QLineEdit* le{ qobject_cast<QLineEdit*>(row->propertyWidget(PT_Technique)) };

                if (technique) {

                    le->setText(toQString(technique->GetName().Split('/').Back().Split('.').Front()));
                    le->setToolTip(toQString(technique->GetName()));

                } else {

                    le->setText("");
                    le->setToolTip("No technique");
                }

                break;

            } case PT_Texture: case PT_ColorAndTexture: {

                TextureWidget* tw{ qobject_cast<TextureWidget*>(row->propertyWidget(PT_Texture)) };

                if (tw) {

                    if (techniqueUsesUnit(technique, tw->textureUnit())) {

                        tw->setVisible(true);

                        if (material) {

                            Texture* texture{ material->GetTexture(tw->textureUnit()) };
                            tw->setTexture(texture);

                        } else {

                            tw->setTexture(nullptr);
                        }
                    } else {

                        tw->setVisible(false);
                    }
                }

                if (row->type() == PT_Texture)
                    break;

            } case PT_Color: {

                ColorWidget* cw{ static_cast<ColorWidget*>(row->propertyWidget(PT_Color)) };
                String parameterName{ toString(cw->property("parameter").toString()) };
                MaterialShaderParameter parameter{};

                if (cw) {
                    if (parameters.TryGetValue(parameterName, parameter)) {

                        Color color{ parameter.value_.GetVector4().Data() };
                        cw->setColor(color);

                    } else {

                        cw->setVisible(false);
                    }
                }
                break;

            } default:
                break;
            }
        }

        updateLabels();

    } else {

        setPropertiesVisible(false);
    }
}

bool PropertiesWidget::techniqueUsesUnit(Technique* technique, TextureUnit unit)
{
    String shortUnitName{ textureUnitNames[unit] };
    shortUnitName.Resize(4);

    for (Pass* p: technique->GetPasses()) {

        if (p->GetPixelShaderDefines().Contains(shortUnitName, false))
            return true;
    }

    return false;
}
