/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#include <QMenuBar>
#include <QToolBar>
#include <QStatusBar>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QLabel>
#include <QSettings>
#include <QFileDialog>
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QUrl>

#include "view3d.h"
#include "materialeditor.h"
#include "texteditor.h"
#include "weaver.h"
#include "urhodockwidget.h"

#include "qocoon.h"

Qocoon::Qocoon(Context* context) : QMainWindow(), Object(context),
    singleWidget_{false}
{
    setWindowTitle(Weaver::applicationName());
    setWindowIcon(QIcon(":/Icon"));

    GetSubsystem<Weaver>()->createScene();
    View3D* centralView{ new View3D(context_) };
    centralView->setScene(GetSubsystem<Weaver>()->getScene());
    setCentralWidget(centralView);

    for (bool right: {false, true}) {

        if (right) {

            View3D* view{ new View3D(context_) };
            UrhoDockWidget* dockWidget{ new UrhoDockWidget(view) };
            view->setScene(GetSubsystem<Weaver>()->getScene());
            addDockWidget(Qt::RightDockWidgetArea, dockWidget);

        } else {

//            for (int i{0}; i < 2; ++i) {

                UrhoDockWidget* materialWidget{ new UrhoDockWidget(new MaterialEditor(context_)) };
                addDockWidget(Qt::LeftDockWidgetArea, materialWidget);

                UrhoDockWidget* textWidget{ new UrhoDockWidget(new TextEditor(context_)) };
                addDockWidget(Qt::LeftDockWidgetArea, textWidget);
//            }
        }
    }

    QToolBar* toolBar{ new QToolBar("Toolbar") };

    for (QString mode: { "Terrain", "Block" }) {

        QAction* action{ new QAction(mode + " Mode", this) };
        action->setIcon(QIcon(":/" + mode ));
        toolBar->addAction(action);
    }

    addToolBar(toolBar);

    createMenuBar();
    setStatusBar(new QStatusBar());
    loadSettings();
    show();
}
Qocoon::Qocoon(Context *context, Material* material) : QMainWindow(), Object(context),
    singleWidget_{true}
{
    MaterialEditor* materialEditor{ new MaterialEditor(context_) };
    setCentralWidget(materialEditor);
    setWindowTitle(materialEditor->objectName());
    setWindowIcon(QIcon(":/Icon"));
    resize(materialEditor->sizeHint() + QSize(50, 400));
    show();
    materialEditor->setFileButtonsVisible(false);
    materialEditor->setMaterial(material);

    QMenu* fileMenu{ createMenuBar() };

    fileMenu->insertActions(fileMenu->actions().last(), materialEditor->fileOperations());
    QToolBar* toolBar{ new QToolBar("Toolbar") };
    toolBar->addActions(materialEditor->fileOperations());
    addToolBar(toolBar);
}

Qocoon::~Qocoon()
{
    if (singleWidget_)
        return;

    QSettings settings{};

    settings.setValue("geometry", saveGeometry());
    settings.setValue("state", saveState());
//    settings.setValue("centralwidget/geometry", centralWidget()->saveGeometry());

//    for (QObject* o: children()) {

//        if (qobject_cast<Maggot*>(o))
//            settings.setValue(o->objectName() + "/geometry", centralWidget()->saveGeometry());
//    }
}
void Qocoon::loadSettings()
{
    QSettings settings{};

    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("state").toByteArray());

//    for (QObject* o: children()) {

//        if (Maggot* m = qobject_cast<Maggot*>(o))
//            m->restoreGeometry(settings.value(o->objectName() + "/geometry").toByteArray());
//    }
//    centralWidget()->restoreGeometry(settings.value("centralwidget/geometry").toByteArray());
}

QMenu* Qocoon::createMenuBar()
{
    Weaver* weaver{ GetSubsystem<Weaver>() };

    QMenu* fileMenu{ new QMenu("File") };
    fileMenu->addAction("Exit", weaver, SLOT(quit()));
    menuBar()->addMenu(fileMenu);

    QMenu* helpMenu{ new QMenu("Help") };
    helpMenu->addAction(tr("About %1...").arg(Weaver::applicationName()), this, SLOT(about()));
    menuBar()->addMenu(helpMenu);

    return fileMenu;
}


void Qocoon::about()
{
    QString aboutText{ tr("<h4>No Copyright - CC0</h4>"
        "<p>The person who associated a work with this deed has dedicated the work to the public domain by waiving all of his or her rights to the work worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.</p>"
        "<p>You can copy, modify, distribute and perform the work, even for commercial purposes, all without asking permission.</p>") };

    QDialog* aboutBox{ new QDialog(this) };

    aboutBox->setWindowTitle("About " + Weaver::applicationName());
    QVBoxLayout* aboutLayout{ new QVBoxLayout() };
    aboutLayout->setContentsMargins(0, 8, 0, 4);

    QPushButton* manawarkButton{ new QPushButton() };
    QPixmap manawarkLogo{ ":/ManaWarg" };
    manawarkButton->setIcon(manawarkLogo);
    manawarkButton->setFlat(true);
    manawarkButton->setMinimumSize(manawarkLogo.width(), manawarkLogo.height());
    manawarkButton->setIconSize(manawarkLogo.size());
    manawarkButton->setToolTip("https://gitlab.com/luckeyproductions/manawarg");
    manawarkButton->setCursor(Qt::CursorShape::PointingHandCursor);
    aboutLayout->addWidget(manawarkButton);
    aboutLayout->setAlignment(manawarkButton, Qt::AlignHCenter);
    connect(manawarkButton, SIGNAL(clicked(bool)), this, SLOT(openUrl()));

    QLabel* aboutLabel{ new QLabel(aboutText) };
    aboutLabel->setWordWrap(true);
    aboutLabel->setAlignment(Qt::AlignJustify);
    QVBoxLayout* labelLayout{ new QVBoxLayout() };
    labelLayout->setContentsMargins(42, 34, 42, 12);
    labelLayout->addWidget(aboutLabel);
    aboutLayout->addLayout(labelLayout);

    QDialogButtonBox* buttonBox{ new QDialogButtonBox(QDialogButtonBox::Ok, aboutBox) };
    connect(buttonBox, SIGNAL(accepted()), aboutBox, SLOT(accept()));
    aboutLayout->addWidget(buttonBox);

    aboutBox->setLayout(aboutLayout);
    aboutBox->resize(aboutBox->minimumSize());
    aboutBox->exec();
}
void Qocoon::openUrl()
{
    QDesktopServices::openUrl(QUrl(qobject_cast<QPushButton*>(sender())->toolTip()));
}
