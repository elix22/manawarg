/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#include <QPushButton>

#include "urhowidget.h"

UrhoWidget::UrhoWidget(Context* context, QWidget *parent) : QWidget(parent), Object(context),
    fileOperations_{},
    buttonLayout_{ new QHBoxLayout() },
    pickerBox_{ nullptr }
{
    for (QString actionName: { "New", "Open", "Save"}) {

        QAction* action{ new QAction(QIcon(":/" + actionName), actionName) };

        if (actionName == "New")
            connect(action, SIGNAL(triggered(bool)), this, SLOT(actionNew()));
        else if (actionName == "Open")
            connect(action, SIGNAL(triggered(bool)), this, SLOT(actionOpen()));
        else if (actionName == "Save")
            connect(action, SIGNAL(triggered(bool)), this, SLOT(actionSave()));

        fileOperations_.push_back(action);
    }
}

void UrhoWidget::createFileButtons()
{
    fileButtonRow_ = new QWidget();
    fileButtonRow_->setLayout(buttonLayout_);
    fileButtonRow_->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    buttonLayout_->setMargin(0);
    buttonLayout_->setSpacing(4);

    for (QAction* action: fileOperations()) {

        QPushButton* fileOperationButton{ new QPushButton() };
        fileOperationButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        fileOperationButton->setIcon(action->icon());
        fileOperationButton->setToolTip(action->text());

        connect(fileOperationButton, SIGNAL(clicked(bool)), action, SLOT(trigger()));
        buttonLayout_->addWidget(fileOperationButton);
    }
}
void UrhoWidget::setFileButtonsVisible(bool visible)
{
    fileButtonRow_->setVisible(visible);
}

void UrhoWidget::setOrientation(Qt::Orientation orientation)
{
    buttonLayout_->setDirection(orientation == Qt::Horizontal ? QBoxLayout::TopToBottom : QBoxLayout::LeftToRight);
}
