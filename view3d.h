/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#ifndef VIEW3DWIDGET_H
#define VIEW3DWIDGET_H

#include "urhowidget.h"

class View3D : public UrhoWidget
{
    Q_OBJECT
    URHO3D_OBJECT(View3D, UrhoWidget)
public:
    explicit View3D(Context* context, QWidget *parent = nullptr);

    void updateView();
    void setScene(Scene* scene);
    void setRenderPath(String renderPathName);

protected:
    void resizeEvent(QResizeEvent*) override;
    void paintEvent(QPaintEvent*) override;
    void mouseMoveEvent(QMouseEvent* event) override;
//    void wheelEvent(QWheelEvent* event)      override;
//    void mouseMoveEvent(QMouseEvent* event)  override;
//    void mousePressEvent(QMouseEvent* event) override;

private:
    void createRenderTexture();

    void updateViewport();
    void updatePixmap();
    void updateView(StringHash, VariantMap&);
    void paintView();
    void paintView(const StringHash, VariantMap&);

    Scene*                  scene_;
    Camera*                 activeCamera_;
    Camera*                 temporaryCamera_;
    String                  renderPathName_;
    SharedPtr<RenderPath>   renderPath_;
    SharedPtr<Texture2D>    renderTexture_;
    SharedPtr<Image>        image_;
    QPixmap                 pixmap_;

    QPoint previousMousePos_;
    void wrapCursor(const QPoint& pos);
};

#endif // VIEW3DWIDGET_H
