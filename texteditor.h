/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#ifndef TEXTEDITOR_H
#define TEXTEDITOR_H

#include "urhowidget.h"

class TextEditor : public UrhoWidget
{
    Q_OBJECT
public:
    explicit TextEditor(Context* context, QWidget *parent = nullptr);

    void setOrientation(Qt::Orientation orientation) override;

protected slots:
    void actionNew() override;
    void actionOpen() override;
    void actionSave() override;

private:
    QVBoxLayout* mainLayout_;

    File* file_;
};

#endif // TEXTEDITOR_H
