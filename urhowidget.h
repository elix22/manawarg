/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#ifndef MAGGOTWIDGET_H
#define MAGGOTWIDGET_H

#include <QAction>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>

#include "urho3d.h"
#include <QWidget>

class UrhoWidget : public QWidget, public Object
{
    Q_OBJECT
    URHO3D_OBJECT(UrhoWidget, Object)
public:
    explicit UrhoWidget(Context* context, QWidget *parent = nullptr);

    void setFileButtonsVisible(bool visible);
    virtual void setOrientation(Qt::Orientation orientation);

    QList<QAction*>& fileOperations() { return fileOperations_; }
    QGroupBox* pickerBox() const { return pickerBox_; }

protected slots:
    virtual void actionNew() {}
    virtual void actionOpen() {}
    virtual void actionSave() {}

protected:
    void createFileButtons();

    QList<QAction*> fileOperations_;
    QHBoxLayout* buttonLayout_;
    QWidget* fileButtonRow_;
    QGroupBox* pickerBox_;
};

#endif // MAGGOTWIDGET_H
