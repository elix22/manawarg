/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#include <QResizeEvent>
#include <QPainter>
#include "weaver.h"
#include "view3d.h"


View3D::View3D(Context* context, QWidget *parent) : UrhoWidget(context, parent),
    scene_{},
    activeCamera_{},
    renderPath_{ GetSubsystem<Renderer>()->GetDefaultRenderPath() },
    renderPathName_{ "RenderPaths/Forward.xml" },
    renderTexture_{},
    image_{},
    previousMousePos_{ width() / 2, height() / 2 }
{
    setObjectName("View 3D");
    setMinimumSize(80, 45);
    setFocusPolicy(Qt::StrongFocus);
    setMouseTracking(true);

    Node* cameraNode{ new Node(context_) };
    cameraNode->SetTemporary(true);
    temporaryCamera_ = cameraNode->CreateComponent<Camera>();
    temporaryCamera_->SetTemporary(true);
    Node* pivot{ cameraNode->CreateChild("Pivot") };
    pivot->Translate(Vector3::FORWARD * 5.0f);
    pivot->SetTemporary(true);

    pixmap_ = QPixmap(width(), height());
    pixmap_.fill(Qt::transparent);
}
void View3D::setScene(Scene* scene)
{
    if (scene == scene_)
        return;

    scene_ = scene;
    activeCamera_ = nullptr;

    Node* cameraNode{ temporaryCamera_->GetNode() };
    for (Node* node: scene_->GetChildrenWithComponent("Camera", true) ) {

        Camera* camera{ node->GetComponent<Camera>() };

        if (!activeCamera_ || camera->GetID() > activeCamera_->GetID())

            activeCamera_ = camera;
    }
    if (!activeCamera_){

        cameraNode->SetScene(scene_);
        cameraNode->SetPosition(Vector3::ONE * 2.3f);
        cameraNode->LookAt(Vector3::ZERO);
        activeCamera_ = temporaryCamera_;

    } else {

        cameraNode->SetParent(activeCamera_->GetNode());
        cameraNode->ResetToDefault();
        temporaryCamera_->SetFov(activeCamera_->GetFov());
    }

    createRenderTexture();
}
void View3D::createRenderTexture()
{
    if (renderTexture_)
        renderTexture_->GetRenderSurface()->Release();

    renderTexture_ = new Texture2D(context_);
    renderTexture_->SetSize(width(), height(), Graphics::GetRGBAFormat(), TEXTURE_RENDERTARGET);
    renderTexture_->GetRenderSurface()->SetUpdateMode(SURFACE_MANUALUPDATE);

    updateViewport();
}
void View3D::setRenderPath(String renderPathName)
{
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    SharedPtr<XMLFile> renderPathFile{ cache->GetResource<XMLFile>(renderPathName) };
    renderPath_->Load(renderPathFile);
}
void View3D::updateViewport()
{
    if (!activeCamera_)
        return;

    RenderSurface* renderSurface{ renderTexture_->GetRenderSurface() };

    if (renderSurface->GetViewport(0)) {
        renderSurface->GetViewport(0)->SetScene(scene_);

    } else {

        if (!renderPath_) {

            if (!renderPathName_.Empty())
                setRenderPath(renderPathName_);
            else
                return;
        }

        SharedPtr<Viewport> viewport{ new Viewport(context_, scene_, temporaryCamera_, renderPath_) };
        renderSurface->SetViewport(0, viewport);
    }

    if (height()) {

        float portraitRatio{ Min(1.0f, static_cast<float>(width()) / height()) };

        if (activeCamera_ != temporaryCamera_)
            temporaryCamera_->SetZoom(activeCamera_->GetZoom() * portraitRatio);
        else
            temporaryCamera_->SetZoom(portraitRatio);
    }

    updateView();
}
void View3D::updateView(StringHash, VariantMap&)
{
    updateView();
}
void View3D::updateView()
{
    if (!renderTexture_)
        return;

    renderTexture_->GetRenderSurface()->QueueUpdate();
    GetSubsystem<Weaver>()->requestUpdate();

    SubscribeToEvent(E_ENDRENDERING, URHO3D_HANDLER(View3D, paintView));
}


void View3D::resizeEvent(QResizeEvent*)
{
    if (!GetSubsystem<Weaver>())
        return;

    createRenderTexture();
}
void View3D::paintEvent(QPaintEvent*)
{
    if (!activeCamera_)
        return;

    paintView();
}
void View3D::paintView(const StringHash, VariantMap&)
{
    repaint();
}
void View3D::paintView()
{
    if (!activeCamera_)
        return;

    updatePixmap();

    int drawWidth{ static_cast<int>(ceil(pixmap_.width() * (static_cast<float>(height()) / pixmap_.height()))) };
    int drawHeight{ static_cast<int>(ceil(pixmap_.height() * (static_cast<float>(width()) / pixmap_.width()))) };
    int dX{ width() - drawWidth };
    int dY{ height() - drawHeight };

    QPainter p{};
    QColor backgroundColor{ toQColor(GetSubsystem<Renderer>()->GetDefaultZone()->GetFogColor(), false) };

    if (scene_) {
        if (Zone* zone = scene_->GetComponent<Zone>()) {
            backgroundColor =  toQColor(zone->GetFogColor());
        }

        if (backgroundColor.alpha() < 255) {

            Weaver::paintCheckerboard(this, 12);
            p.begin(this);
            p.fillRect(rect(), backgroundColor);
            p.end();
        }
    }

    p.begin(this);
    p.drawPixmap(QRect(dX / 2, dY / 2, drawWidth, drawHeight), pixmap_);
    p.end();
}
void View3D::updatePixmap()
{
    Image* image{ renderTexture_->GetImage() };

    if (!image || renderTexture_->GetRenderSurface()->IsUpdateQueued())
        return;

    if (image_ != image) {

        image_  = renderTexture_->GetImage();
        pixmap_ = toPixmap(image_);

        UnsubscribeFromEvent(E_ENDRENDERING);
    }
}

void View3D::mouseMoveEvent(QMouseEvent *event)
{
    if (!scene_)
        return;

    setFocus();

    Qt::KeyboardModifiers modifiers{ event->modifiers() };

//    Vector2 posNormalized{ static_cast<float>(event->pos().x()) / (width() - 1),
//                           static_cast<float>(event->pos().y()) / (height() - 1)};
    QPoint dPos{ QCursor::pos() - previousMousePos_ };
    Vector2 dVec{ dPos.x() * 0.00125f, dPos.y() * 0.001666f };

    Qt::MouseButtons buttons{ QApplication::mouseButtons() };
    if (buttons) {

        Node* modelNode{ scene_->GetChild("Model") };
        if (modelNode && modelNode->IsTemporary()) { //Spin model

            float rotSpeed{ 235.0f };
            float pitchDelta{ -dVec.y_ * rotSpeed };
            modelNode->Rotate(Quaternion(-dVec.x_ * rotSpeed, Vector3::UP) *
                              Quaternion(pitchDelta, activeCamera_->GetNode()->GetRight()), TS_WORLD);

            wrapCursor(event->pos());

        } else if (buttons & Qt::MiddleButton) { //Spin camera

            float rotSpeed{ 123.0f };
            Node* cameraNode{ temporaryCamera_->GetNode() };
            Node* pivot{ cameraNode->GetChild(unsigned(0)) };
            if (cameraNode->GetParent() != nullptr && cameraNode->GetParent() != scene_)
                cameraNode = cameraNode->GetParent();

            cameraNode->RotateAround(pivot->GetWorldPosition(),
                                     Quaternion(dVec.x_ * rotSpeed, Vector3::UP) *
                                     Quaternion(dVec.y_ * rotSpeed, cameraNode->GetRight()),
                                     TS_WORLD);

            wrapCursor(event->pos());
        }
        //        Activate();

        //        if (modifiers & Qt::ShiftModifier && modifiers & Qt::ControlModifier) {
        //            //Change field of view
        //            edddyCam_->Move(Vector3::ONE * dVec.y_, MT_FOV);

        //        } else if (modifiers & Qt::ShiftModifier) {
        //            //Pan camera
        //            edddyCam_->Move(Vector3(dVec.x_, dVec.y_, 0.0f), MT_PAN);

        //        } else if (modifiers & Qt::ControlModifier) {
        //            //Zoom camera
        //            edddyCam_->Move(Vector3::FORWARD * -dVec.y_, MT_PAN);

        //        } else {
        //            //Rotate camera
        //            edddyCam_->Move(Vector3(dVec.x_, dVec.y_, 0.0f), MT_ROTATE);

        //        }


    } else {

//        cursor->HandleMouseMove();
    }

    updateView();
    previousMousePos_ = QCursor::pos();
}
void View3D::wrapCursor(const QPoint& pos)
{
    if (pos.x() <= 0)
        QCursor::setPos(QCursor::pos() + QPoint(width() - 2, 0));

    else if (pos.x() >= width() - 1)
        QCursor::setPos(QCursor::pos() - QPoint(width() - 2, 0));

    else if (pos.y() <= 0)
        QCursor::setPos(QCursor::pos() + QPoint(0, height() - 2));

    else if (pos.y() >= height() - 1)
        QCursor::setPos(QCursor::pos() - QPoint(0, height() - 2));
}
