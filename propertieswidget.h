/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#ifndef PROPERTIESWIDGET_H
#define PROPERTIESWIDGET_H

#include <QFormLayout>
#include <QScrollArea>
#include <QLineEdit>

#include "urho3d.h"
#include "propertyrow.h"

class UrhoWidget;

class PropertiesWidget : public QScrollArea, Object
{
    Q_OBJECT
    URHO3D_OBJECT(PropertiesWidget, Object)
public:
    explicit PropertiesWidget(Context* context, UrhoWidget* parent = nullptr);

    void setResource(Resource* resource);
    void updatePropertyFields();
    void setPropertiesVisible(bool visible);

    UrhoWidget* urhoWidget() { return urhoWidget_; }

signals:
public slots:
    void propertyChanged();

protected:
    void resizeEvent(QResizeEvent*) override;

private slots:
    void pickTechnique();

private:
    void createPropertyFields();

    UrhoWidget* urhoWidget_;

    WeakPtr<Resource> resource_;
    QFormLayout* propertiesLayout_;
    QWidget* containerWidget_;
    std::vector<PropertyRow*> rows_;

    void createPropertyRow(QString text, PropertyType type);
    bool techniqueUsesUnit(Technique* technique, TextureUnit unit);
    void createMaterialPropertyFields();
    void updateLabels();
};

#endif // PROPERTIESWIDGET_H
