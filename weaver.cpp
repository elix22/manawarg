/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#include <QWidget>
#include <QTimer>
#include <QDebug>
#include <QSettings>
#include <QFileDialog>
#include <QMessageBox>
#include "qocoon.h"

#include "weaver.h"

Weaver::Weaver(Context* context, int & argc, char** argv) : QApplication(argc, argv), Application(context),
    argument_{},
    requireUpdate_{ true },
    mainWindow_{ nullptr },
    scene_{ nullptr },
    project_{ nullptr }
{
    std::locale::global(std::locale::classic());

    QCoreApplication::setOrganizationName("LucKey Productions");
    QCoreApplication::setOrganizationDomain("luckeyprodutions.nl");
    QCoreApplication::setApplicationName("ManaWarg");

    if (argc == 2) {

        const String argument{ argv[1] };
        if (GetExtension(argument) == ".xml")
            argument_ = argument;
    }
}
Weaver::~Weaver()
{
    delete mainWindow_;
}

void Weaver::Setup()
{
    QWidget* fishTrap{ new QWidget() };

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("urho3d", "logs") +
                                        toString(applicationName().toLower()) + ".log";

    engineParameters_[EP_RESOURCE_PATHS] = "Resources;";
    engineParameters_[EP_EXTERNAL_WINDOW] = (void*)(fishTrap->winId());
    engineParameters_[EP_WORKER_THREADS] = false;

    delete fishTrap;
}

void Weaver::Start()
{
    SetRandomSeed(GetSubsystem<Time>()->GetSystemTime());
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    cache->SetAutoReloadResources(true);
    context_->RegisterSubsystem(this);

    findUrho();

    if (!argument_.Empty()) {

        handleArgument();
    }

    if (!mainWindow_) {

        mainWindow_ = new Qocoon(context_);
    }

    QTimer timer;
    connect(&timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    timer.start(5);

    exec();
    exit();
}
void Weaver::Stop()
{
    engine_->DumpResources(true);
}
void Weaver::exit()
{
    engine_->Exit();
}

void Weaver::findUrho()
{
    QSettings settings{};

    while (!looksLikeUrho(urhoFolder())) {

        QString folder{ QFileDialog::getExistingDirectory(nullptr, tr("Locate Urho"), QString("/home")) };

        if (folder.isEmpty()) {

            exit();

        } else if (looksLikeUrho(toString(folder))) {

                settings.setValue("folders/urho3d", folder);

        } else {

            QMessageBox* notFoundBox{ new QMessageBox(QMessageBox::Information,
                        tr("Urho not found"),
                        tr("The selected folder was not recognized as containing Urho3D")) };

            notFoundBox->setWindowIcon(QIcon(":/Icon"));
            notFoundBox->exec();
        }
    }

    GetSubsystem<ResourceCache>()->AddResourceDir(urhoFolder() + "/bin/Data");
    GetSubsystem<ResourceCache>()->AddResourceDir(urhoFolder() + "/bin/CoreData");
}
bool Weaver::looksLikeUrho(QString urhoFolder)
{
    return looksLikeUrho(toString(urhoFolder));
}
bool Weaver::looksLikeUrho(String urhoFolder)
{
    FileSystem* fs{ GetSubsystem<FileSystem>() };

    return fs->DirExists(urhoFolder + "/bin/Data")
        && fs->DirExists(urhoFolder + "/bin/CoreData");
}
void Weaver::handleArgument()
{
    FileSystem* fs{ GetSubsystem<FileSystem>() };
    if (!IsAbsolutePath(argument_)) {

        while (argument_.Contains("../")) {

            fs->SetCurrentDir(GetParentPath(fs->GetCurrentDir()));
            argument_ = argument_.Substring(3, argument_.Length() - 3);  ///Is it safe enough?
        }

        argument_ = fs->GetCurrentDir() + argument_;
    }

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    SharedPtr<XMLFile> xmlFile{ cache->GetTempResource<XMLFile>(argument_) };

    if (xmlFile) {

        const XMLElement& rootElem{ xmlFile->GetRoot() };
        String elemName{ rootElem.GetName() };

        if (!elemName.Empty()) {

            if (elemName == "project") {

                locateResourcesRoot(argument_, false);
                mainWindow_ = new Qocoon(context_);


            } else if (elemName == "material") {

                SharedPtr<Material> material{ cache->GetResource<Material>(argument_) };

                if (material.NotNull()) {

                    String resourceRoot{ locateResourcesRoot(argument_, true) };

                    if (resourceRoot.Empty()) {

                        return;
                    }

                    argument_.Replace(resourceRoot, "");
                    material->SetName(argument_);
                    cache->ReloadResource(material);

                    mainWindow_ = new Qocoon(context_, material);
                }
            }
        }
    }
}
String Weaver::locateResourcesRoot(String resourcePath, bool acceptUrhoFolder) ///Requires some splitting up
{
    if (resourcePath.Empty())
        return resourcePath;

    FileSystem* fs{ GetSubsystem<FileSystem>() };

    if (acceptUrhoFolder && resourcePath.Contains(urhoFolder())) {

        return containingFolder(resourcePath);
    }

    String trail{ resourcePath };
    while (trail.Length() > 2) {

        String projectFile{ trail + CONFIGURATIONFILENAME };

        if (fs->FileExists(projectFile)) {

            GetSubsystem<ResourceCache>()->AddResourceDir(trail);
            project_ = GetSubsystem<ResourceCache>()->GetTempResource<XMLFile>(projectFile);
            project_->SetName(projectFile);

            return trail;
        }
        trail = GetParentPath(trail);
    }


    String selectedFolder{ toString(QFileDialog::getExistingDirectory(
                                nullptr, tr("Mark Resources Folder"), toQString(resourcePath))) };

    if (selectedFolder.Empty()) {

        return selectedFolder;

    } else if (resourcePath.Contains(selectedFolder)) {

        XMLFile* marker{ new XMLFile(context_) };
        marker->CreateRoot("project");
        marker->SaveFile(AddTrailingSlash(selectedFolder) + CONFIGURATIONFILENAME);
    }

    return locateResourcesRoot(resourcePath, false);
}

void Weaver::onTimeout()
{
    if (requireUpdate_ && !mainWindow_->isMinimized()
            && engine_ && !engine_->IsExiting()) {

        runFrame();
    }
}
void Weaver::runFrame()
{
    requireUpdate_ = false;
    engine_->RunFrame();

//    qDebug() << GetSubsystem<Time>()->GetTimeStamp().CString() << " Ran frame";
}

void Weaver::createScene()
{
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };

    scene_ = new Scene(context_);

    XMLFile* sceneXML{ cache->GetResource<XMLFile>("Scenes/NinjaSnowWar.xml") };
    scene_->LoadXML(sceneXML->GetRoot("scene"));
//    return;

//    scene_->CreateComponent<Octree>();

//    //Light
//    Node* lightNode{ scene_->CreateChild("Light") };
//    lightNode->SetPosition(Vector3(2.0f, 3.0f, 1.0f));
//    lightNode->CreateComponent<Light>();
    //Camera

    //Box!
//    Model* mushroomModel{ cache->GetResource<Model>("Models/Mushroom.mdl") };

//    StaticModel* mushroom{ scene_->CreateChild()->CreateComponent<StaticModel>() };
//    mushroom->SetModel(mushroomModel);
//    mushroom->SetMaterial(cache->GetResource<Material>("Materials/Mushroom.xml"));
}

String Weaver::urhoFolder() const
{
    QSettings settings{};

    return toString(settings.value("folders/urho3d").toString());
}
String Weaver::urhoDataFolder() const
{
    return urhoFolder() + "/bin/Data";
}
String Weaver::urhoCoreDataFolder() const
{
    return urhoFolder() + "/bin/CoreData";
}

String Weaver::projectLocation() const
{
    if (project_)
        return GetPath(project_->GetName());

    else
        return "";
}

