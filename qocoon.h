/* ManaWarg
No Copyright - CC0 - https://creativecommons.org/publicdomain/zero/1.0/

    The person who associated a work with this deed has dedicated the work
    to the public domain by waiving all of his or her rights to the work
    worldwide under copyright law, including all related and neighboring rights,
    to the extent allowed by law.

    You can copy, modify, distribute and perform the work,
    even for commercial purposes, all without asking permission.

*/

#ifndef QOCOON_H
#define QOCOON_H

#include <QMainWindow>
#include "urho3d.h"

class Qocoon : public QMainWindow, public Object
{
    Q_OBJECT
    URHO3D_OBJECT(Qocoon, Object)
public:
    explicit Qocoon(Context* context);
    explicit Qocoon(Context* context, Material* material);
    ~Qocoon();
private slots:
    void about();
    void openUrl();
private:
    QMenu* createMenuBar();
    void loadSettings();

    bool singleWidget_;
};

#endif // QOCOON_H
